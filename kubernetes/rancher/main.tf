provider "rancher" {
  api_url    = "http://54.169.207.143:8080"

}
resource "rancher_environment" "default" {
  name = "staging"
  description = "The staging environment"
  orchestration = "kubernetes"

}
resource rancher_host "rancher-host" {
  name           = "foo"
  description    = "The foo node"
  environment_id = "${rancher_environment.default.id}"
  hostname       = "rancher-host"
  labels {
    role = "rancher"
  }
}