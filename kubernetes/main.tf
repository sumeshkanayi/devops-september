provider "aws" {
  region = "ap-southeast-1"

access_key=""
secret_key=""
}

variable "base"{

type = "map"

default={

ami = "ami-c60387a5"
instance_type = "t2.medium"
#Modify here with your security 

security_groups = "devops"

#Modify here with your key pair 

key_name="siamol"

tag_name = "rancher"
}

description = "Base configuration variables"

}

resource "aws_security_group" "allow_all" {
  name        = "devops"
  description = "Allow all inbound traffic"

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]

  }
}


resource "aws_iam_role" "rancher_iam_role" {
  name = "rancher"
  assume_role_policy = <<EOF
{
"Version": "2012-10-17",
"Statement": [
{
"Action": "sts:AssumeRole",
"Principal": {
"Service": "ec2.amazonaws.com"
},
"Effect": "Allow",
"Sid": ""
}
]
}
EOF
}

resource "aws_iam_group" "rancher-group" {
  name = "rancher-group"
}

resource "aws_iam_policy" "rancher-polciy" {
  name        = "rancher"
  description = "Rancher policy to have enough permission for rancher vms to create elbs etc"
  policy      =  <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "ec2:Describe*",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "ec2:AttachVolume",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "ec2:DetachVolume",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "ec2:*"
            ],
            "Resource": [
                "*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "elasticloadbalancing:*"
            ],
            "Resource": [
                "*"
            ]
        }
    ]
}
EOF

}

resource "aws_iam_policy_attachment" "attach-group" {
  name       = "rancher-attach"
 
  roles      = ["${aws_iam_role.rancher_iam_role.name}"]
  groups     = ["${aws_iam_group.rancher-group.name}"]
  policy_arn = "${aws_iam_policy.rancher-polciy.arn}"
}

resource "aws_iam_instance_profile" "rancher_server_instance_profile" {

    name = "rancher"
    role = "${aws_iam_role.rancher_iam_role.name}"


}





resource "aws_instance" "rancher" {
  ami = "${lookup(var.base,"ami")}"
  instance_type = "${lookup(var.base,"instance_type")}"
  security_groups = ["${aws_security_group.allow_all.name}"]
  
  tags {
    "Name" = "${lookup(var.base,"tag_name")}"
  }
  key_name = "${lookup(var.base,"key_name")}"


}
output "rancher url"{

value = "http://${aws_instance.rancher.public_ip}:8080"

}
/*
resource "aws_instance" "kubectl" {
  ami = "ami-6f198a0c"
  instance_type = "t2.micro"
  security_groups = ["${aws_security_group.allow_all.name}"]
  count=1

  tags {
    "Name" = "kubelet"
  }
  key_name = "${lookup(var.base,"key_name")}"
  user_data = "${file("installkubectl")}"



}
output "kubectl_machine"{

  value = "kubelet: ssh root@${aws_instance.kubectl.public_ip}"

}

resource "aws_instance" "nfs" {
  ami = "ami-6f198a0c"
  instance_type = "t2.micro"
  security_groups = ["${aws_security_group.allow_all.name}"]
  count=1

  tags {
    "Name" = "nfs"
  }
  key_name = "${lookup(var.base,"key_name")}"
  user_data = "${file("installnfs")}"



}
output "nfs_server_access"{

  value = "nfs server : ssh root@${aws_instance.nfs.public_ip}"

}
output "nfs_server_details"{

  value = "nfs server : ${aws_instance.nfs.private_ip}"
}



*/
