provider "aws" {
  region = "ap-southeast-1"
  access_key=""
  secret_key=""

}


resource "aws_security_group" "allow_all" {
  name        = "devops"
  description = "Allow all inbound traffic"

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]

  }
  lifecycle {
    ignore_changes = [ "name"]
  }
}

data "template_file" "installGitlab" {
  template = "${file("installGitlab.tpl")}"
  vars {
    nfs_server = "${aws_instance.nfs.private_ip}"
  }

}



resource "aws_instance" "gitlab" {
  ami = "ami-6f198a0c"
  instance_type = "t2.medium"
  security_groups = ["devops"]
  count=2


  tags {
    "Name" = "gitlab-server-${count.index+1}"
  }
  key_name = "siamol"
   user_data = "${data.template_file.installGitlab.rendered}"


}




resource "aws_instance" "nfs" {
  ami = "ami-6f198a0c"
  instance_type = "t2.micro"
  security_groups = ["devops"]



  user_data = "${file("installNfs")}"
  tags {
    "Name" = "nfs"
  }
  key_name = "siamol"

}

resource "aws_instance" "postgress" {
  ami = "ami-6f198a0c"
  instance_type = "t2.micro"
  security_groups = ["devops"]




  tags {
    "Name" = "postgress"
  }
  key_name = "siamol"
  user_data = "${file("installDB")}"


}

resource "aws_instance" "haproxy" {
  ami = "ami-6f198a0c"
  instance_type = "t2.micro"
  security_groups = ["devops"]




  tags {
    "Name" = "haproxy"
  }
  key_name = "siamol"
  user_data = "${file("installHA")}"


}



