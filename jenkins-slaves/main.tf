provider "aws" {
  region = "ap-southeast-1"
  access_key=""
  secret_key=""



}


resource "aws_instance" "jenkinsSlave" {
  ami = "ami-6f198a0c"
  instance_type = "t2.micro"
  security_groups = ["devops"]
  tags {
    "Name" = "jenkinsSlave"
  }
  key_name = "siamol"
  user_data = "${file("installjenkinsSlave")}"


}

resource "aws_instance" "jenkinsMaven" {
  ami = "ami-6f198a0c"
  instance_type = "t2.micro"
  security_groups = ["devops"]
  tags {
    "Name" = "jenkinsMaven"
  }
  key_name = "siamol"
  user_data = "${file("installjenkinsMaven")}"


}




resource "aws_instance" "jenkinsWindows" {
  ami = "ami-440b7e27"
  instance_type = "t2.medium"
  security_groups = ["devops"]
  tags {
    "Name" = "jenkinsWindows"
  }
  key_name = "siamol"
  user_data = "${file("installJenkinsWindows")}"


}


