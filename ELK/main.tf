provider "aws" {
  region = "ap-southeast-1"
  access_key=""
  secret_key=""


}


resource "aws_security_group" "allow_all" {
  name        = "devops"
  description = "Allow all inbound traffic"

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]

  }
}


resource "aws_instance" elastic {
  ami = "ami-6f198a0c"
  instance_type = "t2.medium"
  security_groups = ["devops"]

  
  tags {
    "Name" = "elastic"
  }
  key_name = "siamol"
  user_data = "${file("installElastic")}"


}
output "elastic" {
  value = "http://${aws_instance.elastic.public_dns}:9200"
}

resource "aws_instance" "kibana" {
  ami = "ami-6f198a0c"
  instance_type = "t2.micro"
  security_groups = ["devops"]


  tags {
    "Name" = "kibana"
  }
  key_name = "siamol"
  user_data = "${file("installKibana")}"


}
output "kibana" {
  value = "http://${aws_instance.kibana.public_dns}:5601"
}

output "kibana_docker_instruction"{

  value = "ssh root@${aws_instance.kibana.public_dns}  'docker run -it -d --name kibana -p 5601:5601 -e ELASTICSEARCH_URL=http://${aws_instance.elastic.private_ip}:9200 docker.elastic.co/kibana/kibana:6.0.0'"
}
resource "aws_instance" "node" {
  ami = "ami-6f198a0c"
  instance_type = "t2.medium"
  security_groups = ["devops"]
  count = 2


  tags {
    "Name" = "node-${count.index + 1}"
  }
  key_name = "siamol"
  user_data = "${file("installNode")}"


}
output "node" {
  value = "ssh root@${aws_instance.node.*.public_dns}"
}