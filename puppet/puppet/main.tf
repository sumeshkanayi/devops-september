provider "aws" {
  region = "ap-southeast-1"
  access_key=""
  secret_key=""
}


resource "aws_security_group" "allow_all" {
  name        = "puppet"
  description = "Allow all inbound traffic"

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]

  }
  lifecycle {
    ignore_changes = [ "name"]
  }
}


resource "aws_instance" "server" {
  ami = "ami-6f198a0c"
  instance_type = "t2.medium"
  security_groups = ["puppet"]



  tags {
    "Name" = "puppet-server"
  }
  key_name = "siamol"
  user_data = "${data.template_file.installServer.rendered}"


}

data "template_file" "installAgent" {
  template = "${file("installAgent.tpl")}"

  vars {
    PUPPET_PRIVATE_IP= "${aws_instance.server.private_ip}"


  }
}

data "template_file" "installServer" {
  template = "${file("installServer.tpl")}"

}


resource "aws_instance" "mco-server" {
  ami = "ami-6f198a0c"
  instance_type = "t2.micro"
  security_groups = ["puppet"]
  count = 2



  tags {
    "Name" = "mco-server-${count.index+1}"
  }
  key_name = "siamol"
  user_data = "${data.template_file.installAgent.rendered}"


}

resource "aws_instance" "mco-client" {
  ami = "ami-6f198a0c"
  instance_type = "t2.micro"
  security_groups = ["puppet"]




  tags {
    "Name" = "mco-client"
  }
  key_name = "siamol"
  user_data = "${data.template_file.installAgent.rendered}"


}



