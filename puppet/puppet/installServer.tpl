#!/bin/bash
apt-get -y update
curl -O https://apt.puppetlabs.com/puppetlabs-release-pc1-xenial.deb
dpkg -i puppetlabs-release-pc1-xenial.deb
apt-get -y update
apt-get -y install puppetserver
ln -s /opt/puppetlabs/puppet/bin/puppet /usr/local/bin/puppet
ln -s /opt/puppetlabs/bin/mco /usr/local/bin/mco
ln -s /opt/puppetlabs/puppet/bin/gem /usr/local/bin/gem
sed -i.bak 's/2g/3g/g' /etc/default/puppetserver
systemctl start puppetserver
systemctl enable puppetserver
mkdir /deploy
cd /deploy
wget https://s3-ap-southeast-1.amazonaws.com/devopssumesh/sshd_config
cp sshd_config /etc/ssh/sshd_config
echo "root:Root123" | chpasswd
service sshd restart
echo "127.0.0.1 puppet" >> /etc/hosts



