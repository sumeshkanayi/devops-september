#client only node
node 'PRIVATE_DNS_OF_MCO_CLIENT'{


  include mcollective

  file { '/usr/local/bin/mco':

  ensure => link,
  target => '/opt/puppetlabs/bin/mco',
  }



}


#server (All other nodes other than clients"
node 'PRIVATE_DNS_OF_MCO_SERVERS' {
 include mcollective
 file { '/usr/local/bin/mco':

  ensure => link,
  target => '/opt/puppetlabs/bin/mco',
  }

  file { '/tmp/test.txt':

  ensure => present,
  content => "Hi hello world",
  }



}

#puppet server

node "PRIVATE_DNS_OF_PUPPET_SERVER" {
  class{"nats": }
  file { '/usr/local/bin/mco':

  ensure => link,
  target => '/opt/puppetlabs/bin/mco',
  }
}