#!/bin/bash
sudo apt-get -y update
wget https://apt.puppetlabs.com/puppetlabs-release-pc1-xenial.deb
dpkg -i puppetlabs-release-pc1-xenial.deb
apt-get -y update
apt-get -y install puppet-agent
ln -s /opt/puppetlabs/puppet/bin/puppet /usr/local/bin/puppet
systemctl start puppet
systemctl enable puppet
mkdir /deploy
cd /deploy
wget https://s3-ap-southeast-1.amazonaws.com/devopssumesh/sshd_config
cp sshd_config /etc/ssh/sshd_config
echo "root:Root123" | chpasswd
service sshd restart
echo "${PUPPET_PRIVATE_IP}  puppet" >> /etc/hosts
puppet agent -t

