provider "aws" {
  region = "ap-southeast-1"
  access_key=""
  secret_key=""

}


resource "aws_security_group" "allow_all" {
  name        = "ucd"
  description = "Allow all inbound traffic"

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]

  }
  lifecycle {
    ignore_changes = [ "name"]
  }
}


resource "aws_instance" "server" {
  ami = "ami-6f198a0c"
  instance_type = "t2.medium"

  security_groups = ["ucd"]



  tags {
    "Name" = "UCD"
  }
  key_name = "siamol"
  user_data = "${data.template_file.installServer.rendered}"


}




data "template_file" "installServer" {
  template = "${file("installServer.tpl")}"

}

resource "aws_instance" "client" {
  ami = "ami-6f198a0c"
  instance_type = "t1.micro"
  security_groups = ["ucd"]



  tags {
    "Name" = "UCD-CLIENT-${count.index}"
  }
  key_name = "siamol"
  user_data = "${data.template_file.installClient.rendered}"
  count=2



}
data "template_file" "installClient" {
  template = "${file("installClient.tpl")}"
  vars{
    UCD_SERVER = "${aws_instance.server.private_ip}"
  }

}



