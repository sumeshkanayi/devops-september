provider "aws" {
  region = "ap-southeast-1"
  access_key="AKIAID2GCGBEZBVQIZDQ"
  secret_key="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"


}


resource "aws_security_group" "allow_all" {
  name        = "devops"
  description = "Allow all inbound traffic"

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]

  }
}


resource "aws_instance" "prom" {
  ami = "ami-6f198a0c"
  instance_type = "t2.micro"
  security_groups = ["devops"]

  
  tags {
    "Name" = "prom"
  }
  key_name = "siamol"
  user_data = "${file("installProm")}"


}
output "prom_server_webui" {
  value = "http://${aws_instance.prom.public_dns}:9090"
}
output "prom_server_ssh" {
  value = "ssh root@${aws_instance.prom.public_dns}"
}
resource "aws_instance" "grafana" {
  ami = "ami-6f198a0c"
  instance_type = "t2.micro"
  security_groups = ["devops"]


  tags {
    "Name" = "grafana"
  }
  key_name = "siamol"
  user_data = "${file("installGrafana")}"


}
output "grafana_server" {
  value = "http://${aws_instance.grafana.public_dns}:3000"
}
resource "aws_instance" "node" {
  ami = "ami-6f198a0c"
  instance_type = "t2.micro"
  security_groups = ["devops"]
  count = 2


  tags {
    "Name" = "node-${count.index + 1}"
  }
  key_name = "siamol"
  user_data = "${file("installNode")}"


}
output "node" {
  value = "ssh root@${aws_instance.node.public_dns}"
}