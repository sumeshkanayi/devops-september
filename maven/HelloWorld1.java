import java.awt.*;
import java.applet.Applet;

public class HelloWorld extends Applet {
    public void paint(Graphics g) {
        g.drawString("Hello world!", 25, 25);
    }
}