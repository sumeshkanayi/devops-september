
variable "ami" {
  description = "AMI image"
}
variable "instance_type" {
  description = "instance type eg,t1.micro,t2.medium etc"
}
variable "security_group" {
  description = "Seurity group to be part of"
}
variable "server_name" {
  description = "Name of the jenkins slave"
}
variable "key_name" {
  description = "Name of the key pair"
}
variable "user_name" {
  description = "Name of the key pair"
}


data "template_file" "jenkinsSlaveTemplate" {
  template = "${file("jenkinsSlaves/installJenkinsSlave.tpl")}"

  vars {
    userName= "${var.user_name}"
    
  }
}






resource "aws_instance" "jenkinsSlave" {
  ami = "${var.ami}"
  instance_type = "${var.instance_type}"
  security_groups = ["${var.security_group}"]
  
  tags {
    "Name" = "${var.server_name}"
  }
  key_name = "${var.key_name}"
  user_data = "${data.template_file.jenkinsSlaveTemplate.rendered}"


}

output "jenkins_slave_ip" {
  value = "${aws_instance.jenkinsSlave.public_ip}"
}
