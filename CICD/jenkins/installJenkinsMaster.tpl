#!/bin/bash

#SETUP DOCKER AND LOGIN CREDENTIALS FIRST

mkdir /deploy
cd /deploy
curl -fsSL get.docker.com -o get-docker.sh
sh get-docker.sh
service docker restart
wget https://s3-ap-southeast-1.amazonaws.com/devopssumesh/sshd_config

cp sshd_config /etc/ssh/sshd_config

echo "root:Root123" | chpasswd
service sshd restart

usermod -aG docker jenkins

apt-get -y update
apt-get -y install default-jdk
cd /
wget http://www-eu.apache.org/dist/maven/maven-3/3.5.2/binaries/apache-maven-3.5.2-bin.tar.gz
tar -xvzf /apache-maven-3.5.2-bin.tar.gz
ln -s /apache-maven-3.5.2/bin/mvn /usr/local/bin/mvn
wget -q -O - https://pkg.jenkins.io/debian/jenkins-ci.org.key |  apt-key add -
echo deb https://pkg.jenkins.io/debian-stable binary/ |  tee /etc/apt/sources.list.d/jenkins.list
apt-get -y update
apt-get -y install jenkins
systemctl start jenkins
curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
chmod +x
chmod +x ./kubectl

mv ./kubectl /usr/local/bin/kubectl
mkdir /var/lib/jenkins/.kube
reboot