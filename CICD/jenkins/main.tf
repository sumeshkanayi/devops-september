provider "aws" {
  region = "ap-southeast-1"
  access_key = ""
  secret_key = ""


}


resource "aws_security_group" "allow_all" {
  name        = "cicd"
  description = "Allow all inbound traffic"

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]

  }

}


resource "aws_instance" "server" {
  ami = "ami-6f198a0c"
  instance_type = "t2.medium"

  security_groups = ["cicd"]



  tags {
    "Name" = "jenkins"
  }
  key_name = "siamol"
  user_data = "${data.template_file.installServer.rendered}"


}




data "template_file" "installServer" {
  template = "${file("installJenkinsMaster.tpl")}"

}










