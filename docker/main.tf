provider "aws" {
  region = "ap-southeast-1"
  access_key=""
  secret_key=""


}


resource "aws_instance" "docker" {
  ami = "ami-6f198a0c"
  instance_type = "t2.micro"
  security_groups = ["devops"]
  
  tags {
    "Name" = "docker"
  }
  key_name = "siamol"
  user_data = "${file("installDocker")}"


}
